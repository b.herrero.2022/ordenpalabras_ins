import sys

def is_lower(first: str, second: str):
    lower = False
    for position in range(len(first)):
        if position < len(second):
            if first[position].lower() < second[position].lower():
                lower = True
                break
            if first[position].lower() > second[position].lower():
                break
    return lower

def sort_pivot(words: list, position: int):
    lower: int = position
    for x in range(position + 1, len(words)):
        if is_lower(words[x], words[lower]):
            x, lower = lower, x
    return lower

def sort(words: list):
    for pivot in range(len(words)):
        lower = sort_pivot(words, pivot)
        if lower != pivot:
            words[lower], words[pivot] = words[pivot], words[lower]
    return words

def show(words: list):
    for words in words:
        print(words, end=' ')
    print()

def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()
